package it.priscofrancesco.pascalinmc.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import java.util.List;

import it.priscofrancesco.pascalinmc.Configs;
import it.priscofrancesco.pascalinmc.Model.Match;
import it.priscofrancesco.pascalinmc.R;


public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.MyViewHolder> {

    private List<Match> matchList;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img_sq_casa,img_sq_ospite;
        public TextView nome_sq_casa, nome_sq_ospite,punteggio,data,orario;


        public MyViewHolder(View view) {
            super(view);
            img_sq_casa = (ImageView)  view.findViewById(R.id.img_sq_casa);
            nome_sq_casa = (TextView) view.findViewById(R.id.nome_sq_casa);

            img_sq_ospite = (ImageView)  view.findViewById(R.id.img_sq_ospite);
            nome_sq_ospite = (TextView) view.findViewById(R.id.nome_sq_ospite);

            punteggio = (TextView) view.findViewById(R.id.punteggio);
            data= (TextView) view.findViewById(R.id.match_data);
            orario = (TextView) view.findViewById(R.id.match_orario);

        }
    }


    public MatchAdapter(List<Match> matchList) {
        this.matchList = matchList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.match_row, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Match match = matchList.get(position);

        Picasso.get().load(Configs.URL+"/"+ Configs.IMAGE_DIR+"/"+match.getUrlImmagineCasa()).into(holder.img_sq_casa);
        holder.nome_sq_casa.setText(match.getNomeSquadraCasa());


        Picasso.get().load(Configs.URL+"/"+ Configs.IMAGE_DIR+"/"+match.getUrlImmagineOspite()).into(holder.img_sq_ospite);
        holder.nome_sq_ospite.setText(match.getNomeSquadraOspite());

        holder.punteggio.setText(match.getGoalCasa() + " - "+match.getGoalOspite());
        holder.data.setText("Data: "+match.getData());
        holder.orario.setText("Ora: "+match.getOra());
    }

    @Override
    public int getItemCount() {
        return matchList.size();
    }

}