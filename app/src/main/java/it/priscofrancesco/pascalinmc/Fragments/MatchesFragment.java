package it.priscofrancesco.pascalinmc.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.priscofrancesco.pascalinmc.Adapter.MatchAdapter;
import it.priscofrancesco.pascalinmc.AppController;
import it.priscofrancesco.pascalinmc.Configs;
import it.priscofrancesco.pascalinmc.Model.Match;
import it.priscofrancesco.pascalinmc.R;


public class MatchesFragment extends Fragment {
    private List<Match> matchList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MatchAdapter mAdapter;
    private ProgressDialog pDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        return root;
    }
    @Override
    public void onViewCreated(View view,  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Calendario Matches");
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        mAdapter = new MatchAdapter(matchList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        getMatches();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideDialog();
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void getMatches()
    {
        // Tag usato per annullare la richiesta
        String tag_string_req = "req_register";
        pDialog.setMessage("Caricamento...");
        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                Configs.URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideDialog();
                Log.wtf("response",response);
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONArray a = jObj.getJSONArray("matches");
                        for(int i =0;i<a.length();i++)
                        {
                            JSONObject item = a.getJSONObject(i);
                            Match match = new Match();
                            match.setNomeSquadraCasa(item.getString("nome_sq_casa"));
                            match.setUrlImmagineCasa(item.getString("url_immagine_casa"));
                            match.setGoalCasa(item.getString("goal_casa"));

                            match.setNomeSquadraOspite(item.getString("nome_sq_ospite"));
                            match.setGoalOspite(item.getString("goal_ospite"));
                            match.setUrlImmagineOspite(item.getString("url_immagine_ospite"));
                            match.setData(item.getString("data"));
                            match.setOra(item.getString("ora"));
                            matchList.add(match);
                        }
                    } else {
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                //inviando i parametri all'url della registrazone
                Map<String, String> params = new HashMap<String, String>();
                params.put("tag", "getmatches");
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}