package it.priscofrancesco.pascalinmc.Model;

public class Match {
    String nomeSquadraCasa;
    String urlImmagineCasa;
    String goalCasa;

    String nomeSquadraOspite;
    String urlImmagineOspite;
    String goalOspite;

    String data;
    String ora;


    public void setData(String data) {
        this.data = data;
    }

    public void setGoalCasa(String goalCasa) {
        this.goalCasa = goalCasa;
    }

    public void setGoalOspite(String goalOspite) {
        this.goalOspite = goalOspite;
    }

    public void setNomeSquadraCasa(String nomeSquadraCasa) {
        this.nomeSquadraCasa = nomeSquadraCasa;
    }

    public void setNomeSquadraOspite(String nomeSquadraOspite) {
        this.nomeSquadraOspite = nomeSquadraOspite;
    }

    public void setOra(String ora) {
        this.ora = ora;
    }

    public void setUrlImmagineCasa(String urlImmagineCasa) {
        this.urlImmagineCasa = urlImmagineCasa;
    }

    public void setUrlImmagineOspite(String urlImmagineOspite) {
        this.urlImmagineOspite = urlImmagineOspite;
    }

    public String getData() {
        return data;
    }

    public String getGoalCasa() {
        return goalCasa;
    }

    public String getGoalOspite() {
        return goalOspite;
    }

    public String getNomeSquadraCasa() {
        return nomeSquadraCasa;
    }

    public String getNomeSquadraOspite() {
        return nomeSquadraOspite;
    }

    public String getUrlImmagineCasa() {
        return urlImmagineCasa;
    }

    public String getUrlImmagineOspite() {
        return urlImmagineOspite;
    }

    public String getOra() {
        return ora;
    }
}
